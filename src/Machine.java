import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Machine {

    //Variable declaration
    private static double vat;
    private static int total = 0;
    private JFrame frame2;
    private JPanel menu;
    private JTabbedPane tabbedPane1;
    private JPanel Food;
    private JPanel Drink;
    private JButton samonButton;
    private JButton yakinikuButton;
    private JButton udonButton;
    private JButton gyozaButton;
    private JButton ramenButton;
    private JButton friedRiceButton;
    private JButton cokeButton;
    private JButton orangeJuceButton;
    private JButton greenTeaButton;
    private JButton coffeeButton;
    private JTextArea orderList;
    private JButton checkOutButton;
    private JTextArea totalPrice;
    private JTextArea taxRate;

    public Machine(double tax,String takeOut){
        //set tax rate into static variable
        vat = tax;

        //Set menu button infomation
        setButtonInfo(samonButton,"./img/samon.jpg","GrilledSamon",600);
        setButtonInfo(yakinikuButton,"./img/yakiniku.jpg","yakiniku",900);
        setButtonInfo(ramenButton,"./img/ramen.jpg","Ramen",700);
        setButtonInfo(friedRiceButton,"./img/friedRice.jpg","FriedRice",550);
        setButtonInfo(udonButton,"./img/udon.jpg","Udon",400);
        setButtonInfo(gyozaButton,"./img/gyoza.jpg","Gyoza",750);
        setButtonInfo(cokeButton,"./img/coke.jpg","Coke",120);
        setButtonInfo(orangeJuceButton,"./img/oj.png","OrangeJuce",110);
        setButtonInfo(coffeeButton,"./img/coffee.jpg","Coffee",190);
        setButtonInfo(greenTeaButton,"./img/greenTea.jpg","GreenTea",100);

        taxRate.setText(takeOut+"\n Tax rate is "+tax);

        //Create new frame
        frame2 = new JFrame("Machine");
        frame2.setContentPane(menu);
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.pack();
        frame2.setVisible(true);

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //Check out confirm action
                int checkOut = JOptionPane.showConfirmDialog(null,
                        "Would you lide check out?","Check out confirm",
                        JOptionPane.YES_NO_OPTION);
                if(checkOut == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + total + " yen.",
                            "Check out message",
                            JOptionPane.INFORMATION_MESSAGE);

                    //Reset data
                    total = 0;
                    orderList.setText("");
                    totalPrice.setText("Total " + total + " yen");

                }
            }
        });
    }

    //Process the order
    public void order(String menuName, int price){

        //Calculate price including tax
        int taxInclude = price+(int)((double)price*vat);

        //Calculate total price and display in view
        total += taxInclude;
        orderList.append(menuName + ": " + taxInclude + " yen\n");
        totalPrice.setText("Total " + total + " yen");

    }

    public void setButtonInfo(JButton button,String imgString,String menuName,int price){

        //Set icon into button
        ImageIcon icon = new ImageIcon(imgString);
        button.setIcon(icon);

        //Menu button event
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                int conf = JOptionPane.showConfirmDialog(null,
                        "Would you like to order"+menuName, "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                //If selected YES, process the order
                if(conf == 0){
                   order(menuName,price);
                }
            }
        });
    }

}