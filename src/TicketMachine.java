import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class TicketMachine {
    private JFrame frame;
    private JButton takeOutButton;
    private JButton eatInButton;
    private JPanel root;

    public TicketMachine() {
        ImageIcon to_icon = new ImageIcon("./img/takeOut.jpg");
        takeOutButton.setIcon(to_icon);
        ImageIcon ei_icon = new ImageIcon("./img/eatIn.jpg");
        eatInButton.setIcon(ei_icon);

        frame = new JFrame("TicketMachine");
        frame.setLocation(70,70);
        //frame.setSize(300, 200);
        //frame.setLocationRelativeTo(null);
        frame.setContentPane(root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        takeOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Machine(0.08,"Take out");
                frame.setVisible(false);

            }
        });

        eatInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Machine(0.1,"Eat in");
                frame.setVisible(false);
            }
        });
    }

    public static void main(String[] args) {
        new TicketMachine();
    }
}
